rec {
  removeParentDirs = orig: dir: dirs:
    if dir == /. then
      dirs
    else if builtins.any (d: d.path == dir) dirs then
      map
      (d: if d.path == dir then d // { remove = d.remove ++ [ orig ]; } else d)
      dirs
    else
      removeParentDirs orig (dirOf dir) dirs;

  applyRemove = map (d:
    let
      clean = builtins.filterSource
        (n: t: !(builtins.any (x: n == toString x) d.remove));
    in clean (d.path));

  removeSubdirs' = dirs:
    let
      dirs' = map (d: {
        remove = [ ];
        path = d;
      }) dirs;
    in builtins.foldl' (acc: x: removeParentDirs x (dirOf x) acc) dirs' dirs;
  removeSubdirs = dirs: applyRemove (removeSubdirs' dirs);
}

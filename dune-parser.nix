src:
let
  orig_lst = import ./sexp.nix "${src}/dune";

  includes = filter "include" orig_lst;

  print = x: builtins.trace (builtins.deepSeq x x) x;

  findAtom = pred: elem:
    if builtins.isList elem then
      builtins.concatMap findAtom elem
    else if pred elem then
      [ elem ]
    else
      [ ];

  lst =
    builtins.concatLists (map (n: import ./sexp.nix "${src}/${arg n}") includes)
    ++ orig_lst;

  filter = name:
    builtins.filter (x: builtins.isList x && builtins.head x == name);
  find = name: lst:
    let filtered = filter name lst;
    in if filtered == [ ] then [ name ] else builtins.head filtered;
  arg = lst: builtins.elemAt lst 1;
in map (comp: rec {
  name = arg (find "name" comp);
  type = builtins.head comp;
  external_paths = findAtom (x: ! isNull (builtins.match "\\.\\./.*" x)) lst;
  public_name = let tail = builtins.tail (find "public_name" comp);
  in if tail == [ ] then
    [ ]
  else
    builtins.filter builtins.isString
    (builtins.split "\\." (builtins.head tail));
  deps = builtins.tail (find "libraries" comp)
    ++ (builtins.filter (n: n != "--conditional")
      (builtins.tail (find "pps" (builtins.tail (find "preprocess" comp)))));
}) (filter "library" lst ++ filter "executable" lst)

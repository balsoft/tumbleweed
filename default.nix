{ src, sources ? import ./nix/sources.nix, pkgs ? import sources.nixpkgs { }
, opam-nix ? import sources.opam-nix pkgs }:
self: super:
let
  comp-builder = import ./comp-builder.nix;
  isDunePackage = buildPhase: !isNull (builtins.match ".*dune.*" buildPhase);
  preprocessing = buildPhase:
    builtins.head (builtins.match ''(.*)"dune.*'' buildPhase);
  preprocess = pkg:
    pkg.overrideAttrs ({ buildPhase, ... }: {
      buildPhase = preprocessing buildPhase;
      installPhase = "cp -r . $out";
    });

  release = name: pkg:
    pkg.overrideAttrs (_: {
      postBuild = "dune build -p ${name}";
      installPhase =
        "mkdir -p $OCAMLFIND_DESTDIR; cp -Lr _build/install/default/lib/${name} $OCAMLFIND_DESTDIR";
    });

  buildDiscardContext = dir:
    builtins.deepSeq (builtins.readDir dir)
    (builtins.unsafeDiscardStringContext dir);
  projects = import ./dune-traverser.nix src;
  dunePackages = builtins.listToAttrs (builtins.concatMap (proj:
    builtins.concatMap (comp:
      let
        fullSrc = src + proj.src + comp.src;
        opamFileName = if builtins.length comp.public_name > 0 then
          builtins.head comp.public_name
        else
          comp.name;
        opamPackage =
          (opam-nix.callOPAMPackage self super).callOPAMPackage fullSrc {
            name = builtins.head comp.public_name;
            pname = builtins.head comp.public_name;
            opamFile = "${opamFileName}.opam";
          } { };
        isOpam = comp.public_name != [ ]
          && (builtins.elem "${opamFileName}.opam"
            (builtins.attrNames (builtins.readDir fullSrc)));
        preprocessedSrc = builtins.path {
          name = comp.name;
          path = buildDiscardContext
            (builtins.filterSource (n: t: t == "regular") (buildDiscardContext
              (if isOpam then
                (preprocess opamPackage) + comp.src
              else
                fullSrc)));
        };
        comp' = if isOpam then
          builtins.head (builtins.filter (c: c.name == comp.name)
            (import ./dune-parser.nix "${preprocessedSrc}"))
        else
          comp;
        component = comp-builder {
          inherit (comp') name deps;
          src = preprocessedSrc;
          inherit opamFileName;
          extraInputs = if isOpam then
            opamPackage.buildInputs ++ opamPackage.nativeBuildInputs
          else
            [ ];
            # extraInputs = [];
          opamFile = src + proj.src + "/${opamFileName}.opam";
        };
        pkg = (self.callPackage component { }) // {
          passthru = {
            inherit proj;
            comp = comp';
          };
        };
        public_name = builtins.concatStringsSep "." comp.public_name;
      in [{
        name = comp.name;
        value = pkg;
      }] ++ pkgs.lib.optional (isOpam && public_name != "") rec {
        name = public_name;
        value = release name pkg;
      }) (builtins.attrValues proj.components)) (builtins.attrValues projects));
in dunePackages // super

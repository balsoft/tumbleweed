rec {
  subdirs = where: relative:
    map (x: relative + (/. + x.name))
    (builtins.filter (x: x.type == "directory") (builtins.attrValues
      (builtins.mapAttrs (name: type: { inherit name type; })
        (builtins.readDir (where + relative)))));

  findFiles = name: stopAt: where: relative:
    let
      contents = builtins.readDir (where + relative);
      hasFile = builtins.elem name (builtins.attrNames contents);
      hasStopFile = builtins.elem stopAt (builtins.attrNames contents);
    in if hasStopFile && relative != /. then
      [ ]
    else
      ((if hasFile then [ relative ] else [ ])
        ++ (builtins.concatMap (findFiles name stopAt where)
          (subdirs where relative)));

  removeSame =
    builtins.foldl' (acc: x: if builtins.elem x acc then acc else acc ++ [ x ])
    [ ];

  duneFiles = src:
    map (p: {
      srcs = findFiles "dune" "dune-project" (src + /. + p) /.;
      orig = p;
    }) (findFiles "dune-project" null src /.);

  projectsList = src:
    map (p: {
      src = p.orig;
      comps = builtins.concatMap (d:
        map (x: x // { src = d; })
        (import ./dune-parser.nix (src + (/. + p.orig + (/. + d))))) p.srcs;
    }) (duneFiles src);

  projects = src:
    builtins.listToAttrs (map ({ comps, src }:
      let
        main_dune_candidates =
          builtins.filter (dune: builtins.length dune.public_name == 1) comps;
        main_dune = if builtins.length main_dune_candidates == 0 then
          builtins.throw
          "Unable to find any components with an unscoped public_name for ${
            toString src
          }"
        else
          builtins.head main_dune_candidates;
      in {
        name = main_dune.name;
        value = rec {
          inherit src;
          components = builtins.listToAttrs (map (dune: {
            inherit (dune) name;
            value = dune;
          }) comps);
        };
      }) (projectsList src));

  __functor = _: projects;
}

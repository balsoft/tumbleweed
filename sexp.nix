file:
let
  postprocess = builtins.replaceStrings [
        "("
        ")"
        "\\"
        "\${"
        "'';"
      ] [
        "["
        "]"
        "\\\\"
        "''\${"
        "#"
      ];
  split = builtins.split "([() \n]+)";
  quote = x: "''${x}''";
  sexpToNix = sexp:
    postprocess (builtins.concatStringsSep "" (map (x:
      if builtins.isString x then
        if x != "" then quote x else ""
      else
        builtins.head x) (split sexp)));
in import
(builtins.toFile "sexp.nix" ("[" + (sexpToNix (builtins.readFile file)) + "]"))

{ name, src, deps, opamFile, opamFileName, extraInputs }: {
  __functor = self: args:
    with args;
    stdenv.mkDerivation rec {
      inherit name;
      buildInputs = [ ocaml findlib rsync ] ++ extraInputs
        ++ (map (dep: args.${dep}) deps);
      propogatedBuildInputs = buildInputs;
      inherit src;
      buildPhase = ''
        runHook preBuild
        cp ${opamFile} ${opamFileName}.opam
        mkdir -p .cache
        export HOME=/build
        DUNE_PROFILE=release
        for dep in ${toString (map (dep: args.${dep}) deps)}
        do
          [ -d $dep/_cache ] && rsync -r $dep/_cache .
        done
        ${dune_2}/bin/dune build --cache=enabled --cache-transport=direct --cache-duplication=copy --root=. ${name}.{a,cma,cmxa,cmxs}
        runHook postBuild
      '';
      installPhase = "cp -r . $out";
    };
  __functionArgs = {
    ocaml = true;
    stdenv = true;
    dune_2 = true;
    opaline = true;
    findlib = true;
    rsync = true;
  } // (builtins.listToAttrs (map (dep: {
    name = dep;
    value = true;
  }) deps));
}
